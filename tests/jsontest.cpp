// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only

#include <QTest>

#include "reflection.h"
#include "json.h"

using namespace desert;

struct EmptyObject {
    DESERT_OBJECT
};

struct SubSubObject {
    DESERT_OBJECT

    Attribute<u"id", int>                  id = 0;
    Attribute<u"ints", std::vector<int>>   ints;
    Attribute<u"empty", EmptyObject>       empty{};
    Attribute<u"map", QHash<QString, int>> map;
    Attribute<u"map2", QHash<QString, EmptyObject>> map2;
};

struct SubObject {
    DESERT_OBJECT

    Attribute<u"id", int>            id = 0;
    Attribute<u"count", int>         count = 0;
    Attribute<u"path", QString>      path;
    Attribute<u"test", SubSubObject> test;
};

struct TestObject {
    DESERT_OBJECT

    Attribute<u"id", int>                      id = 0;
    Attribute<u"name", QString>                name;
    Attribute<u"content", SubObject>           object;
    Attribute<u"children", QVector<SubObject>> children;
};

class JsonTest : public QObject {
    Q_OBJECT

private:
    Q_SLOT void testDeserializeSerialize() {
        QByteArray string =
R"({
    "children": [
        {
            "count": 2,
            "id": 3,
            "path": "hi",
            "test": {
                "empty": {
                },
                "id": 0,
                "ints": [
                    1,
                    2,
                    3,
                    4
                ],
                "map": {
                },
                "map2": {
                }
            }
        },
        {
            "count": 2,
            "id": 4,
            "path": "ho",
            "test": {
                "empty": {
                },
                "id": 1,
                "ints": [
                ],
                "map": {
                },
                "map2": {
                }
            }
        }
    ],
    "content": {
        "count": 2,
        "id": 2,
        "path": "hi",
        "test": {
            "empty": {
            },
            "id": 2,
            "ints": [
            ],
            "map": {
            },
            "map2": {
            }
        }
    },
    "id": 2,
    "name": "Test"
}
)";
        auto deserialized = json::deserialize<TestObject>(string);
//        qDebug() << deserialized.name
//                 << deserialized.id
//                 << deserialized.object->path
//                 << deserialized.children->at(1).id.value;

//        QCOMPARE(deserialized.children->size(), 2);

        QByteArray out = json::toBytes(json::serialize(deserialized));
        qDebug().noquote() << out;
        QCOMPARE(string, out);
    }
};

QTEST_GUILESS_MAIN(JsonTest)

#include "jsontest.moc"
