# Desert

Desert provides deserializable and serializable types for C++.

It allows giving names to struct attributes, for example to read the attributes from a json object's attributes.

The attribute names only exist in the struct at compile time, the resulting struct will use just as much memory as it would without the `Attribute` wrappers.

```cpp
struct Data {
    DESERT_OBJECT
    Attribute<"title", QString> text;
    Attribute<"text", QString> text;
}
```

This concept can be used for very different things:

## JSON

Json can be deserialized very easily, using the provided deserialize function.

```cpp
desert::json::deserialize<Data>(QByteArrayLiteral(R"(
{
	"title": "Hello!",
	"text": "Structs can be deserialized from json!"
}
)"))

Serialization works very similarly.
```

## Models

In many QML applications, `QAbstractListModel` subclasses allow conviniently accessing data from a backend, by only accessing the attributes and rows that are currently needed.

Sometimes these sublcasses can look very similar to each other, and are very minimal.

Desert already has all the information a model needs. An `AutoListModel` of a list the aforementioned `Data` struct could look like this:

```cpp
class DataModel : public AutoListModel<Data> {
    Q_OBJECT
}
```

The `roleNames`, `data` and `rowCount` functions are already automatically implemented.
Role names are just taken from the attribute names.

Data can be added to the model, by calling `updateData`, with a vector of new `Data` objects.
`AutoListModel` will automatically figure out the differences, and signal them to the GUI.
This is based on KDABs awesome [UpdateableModel](https://github.com/KDAB/KDToolBox/tree/master/qt/model_view/updateableModel).
