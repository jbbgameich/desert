#!/usr/bin/env bash

find src/pfr_non_boost -type f -exec sed -i 's/namespace pfr/namespace desert::detail::pfr/g;s/ ::pfr/ ::desert::detail::pfr/g' {} \;
