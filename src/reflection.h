// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only

#include <QStringView>

#include <concepts>

#include <pfr.hpp>

#pragma once

namespace desert {

template <typename T>
concept IsDesertObject = requires(T v)
{
    { T::desertObject() } -> std::same_as<void>;
};

namespace detail {
template <typename T>
concept IsArray = requires(T v, int n)
{
    { v.begin() };
    { v.end() };
    { v.reserve(n) };
    { v.front() };
};

template <typename T>
concept IsMap = requires(T o, typename T::key_type k) {
    { o.value(k) };
};

template <typename T>
concept IsString = requires(T v)
{
    { v.trimmed() };
};

template <typename T>
requires IsDesertObject<T>
constexpr bool isObject() {
    return true;
}

template <typename T>
requires (!IsDesertObject<T>)
constexpr bool isObject() {
    return false;
}

template <typename T>
requires (IsArray<T> && !IsString<T>)
constexpr bool isArray() {
    return true;
}

template <typename T>
requires (!IsArray<T> || IsString<T>)
constexpr bool isArray() {
    return false;
}

template <typename T>
requires (IsMap<T>)
constexpr bool isMap() {
    return true;
}

template <typename T>
    requires (!IsMap<T>)
constexpr bool isMap() {
    return false;
}

}

///
/// Mark an object as serializable / deserializable
///
#define DESERT_OBJECT \
public: \
    static void desertObject() {};

template<size_t N>
struct StringLiteral {
    constexpr StringLiteral(const char16_t (&str)[N]) {
        std::copy_n(str, N, value);
    }

    char16_t value[N];
};

///
/// \brief The Attribute class gives a value a name that can be used for serialization and deserialization.
/// The type of the value needs to implement the following operators:
///  * `==`
///  * `<=>`, or `<=`
///
/// Example usage:
/// ```
/// struct Data {
///     DESERT_OBEJECT
///
///     Attribute<"name", QString> name;
///     Attribute<"timestamp", int64_t> timestamp;
///     ...
/// }
/// ```
///
/// Attribute<..., T> tries to behave as much as possible like the value contained in it,
/// so you can still pass it to functions that expect T, call functions of T or assign values of type T to it.
///
template <StringLiteral name, typename T>
struct Attribute {
    /// \brief Name of the attribute
    [[nodiscard]] constexpr inline QStringView attributeName() const {
        return QStringView(name.value);
    }

    /// A refence to the value contained in the attribute;
    constexpr inline const T &inner() const {
        return value;
    }

    constexpr operator const T &() const {
        return value;
    }

    constexpr const T *operator->() const {
        return &value;
    }

    constexpr const T &operator*() const { return value; }

    Attribute<name, T> &operator=(T &&other) {
        value = other;
        return *this;
    }

    Attribute<name, T> &operator=(const T &other) {
        value = other;
        return *this;
    }

    /// Construct an Attribute from a value, consuming the value
    constexpr Attribute(T &&value) : value(std::move(value)) {}
    /// Construct an Attribute by copying a value
    constexpr Attribute(const T &value) : value(value) {}
    /// Construct an Attribute by creating a default value
    constexpr Attribute() = default;

    T value;
};

namespace detail {
// Helpers for iterating over tuples
template <typename Struct, typename Func, std::size_t i>
inline constexpr void iterate_impl(Struct &tup, Func fun)
{
    using StructType = std::decay_t<Struct>;
    if constexpr(i >= pfr::tuple_size<StructType>::value) {
        return;
    } else {
        fun(pfr::get<i>(tup));
        return iterate_impl<Struct, Func, i + 1>(tup, fun);
    }
}
}

///
/// \brief Iterate over a struct, calling the given function for every attribute it has.
/// \param any value of a struct type
/// \param function with one parameter that is passed each attribute
///
template <typename Struct, typename Func>
inline constexpr void iterateStruct(Struct &tup, Func fun)
{
    detail::iterate_impl<Struct, Func, 0>(tup, fun);
}

}
