// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only

#include "json.h"

QByteArray desert::json::toBytes(const QJsonValue &value) {
    QJsonDocument doc;
    if (value.isArray()) {
        doc.setArray(value.toArray());
    } else {
        doc.setObject(value.toObject());
    }
    return doc.toJson();
}
