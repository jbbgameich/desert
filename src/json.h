// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only

#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include <cmath>
#include <concepts>

#include "reflection.h"

#include <desert_export.h>

#pragma once

namespace desert::json {

namespace detail {

template <typename Value>
QHash<QString, Value> decodeMap(const QJsonValue &value);

template <typename T>
T decodeJsonValue(const QJsonValue &value) {
    if constexpr (std::is_same_v<T, int>) {
        return value.toInt();
    } else if constexpr (std::is_same_v<T, int8_t>) {
        return value.toInt();
    } else if constexpr (std::is_same_v<T, int16_t>) {
        return value.toInt();
    } else if constexpr (std::is_same_v<T, int32_t>) {
        return value.toInt();
    } else if constexpr (std::is_same_v<T, int64_t>) {
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        return value.toInteger();
#else
        return std::floor(value.toDouble());
#endif
    } else if constexpr (std::is_same_v<T, float>) {
        return value.toDouble();
    } else if constexpr (std::is_same_v<T, double>) {
        return value.toDouble();
    } else if constexpr (std::is_same_v<T, QString>) {
        return value.toString();
    } else if constexpr (std::is_same_v<T, bool>) {
        return value.toBool();
    } else {
        return value.toVariant().value<T>();
    }
}

}

///
/// \brief Decode a `QJsonValue` and put it into an object of type T.
/// \param value Value to deserialize
/// \return Decoded object
///
template <typename T>
requires desert::IsDesertObject<T>
T deserialize(const QJsonValue &value) {
    T obj;
    auto object = value.toObject();

    desert::iterateStruct(obj, [&](auto &attribute) {
        using Attr = std::decay_t<decltype(attribute.value)>;
        auto subObj = object.value(attribute.attributeName());

        // The attribute is a json object
        if constexpr (desert::detail::isObject<Attr>()) {
            attribute.value = deserialize<Attr>(subObj.toObject());

        // The attribute is a json array
        } else if constexpr (desert::detail::isArray<Attr>()) {
            const auto array = subObj.toArray();
            using ElementType = std::decay_t<decltype(attribute.value.front())>;
            std::ranges::transform(array, std::back_inserter(attribute.value),
                           [](const QJsonValue &value) {
                if constexpr (desert::detail::isObject<ElementType>() || desert::detail::isArray<ElementType>()) {
                    return deserialize<ElementType>(value);
                } else {
                    return detail::decodeJsonValue<ElementType>(value);
                }
            });

        // The attribute is a map
        } else if constexpr (desert::detail::isMap<Attr>()) {
            attribute.value = detail::decodeMap<typename Attr::mapped_type>(subObj);

        // The attribute is a json value
        } else {
            attribute.value = detail::decodeJsonValue<Attr>(subObj);
        }
    });

    return obj;
}

template <typename T>
requires desert::detail::IsArray<T>
T deserialize(const QJsonValue &value) {
    T out;
    const auto array = value.toArray();
    out.reserve(array.size());

    std::ranges::transform(array, std::back_inserter(out), [](const auto &item) {
        using ElementType = std::decay_t<decltype(out.front())>;
        return deserialize<ElementType>(item);
    });

    return out;
}

template <typename T>
T deserialize(const QJsonValue &value) {
    return detail::decodeJsonValue<T>(value);
}

///
/// \brief Decode a json string and put the result into an object of type T.
/// \param json String to deserialize
/// \return Decoded object
///
template <typename T>
requires (IsDesertObject<T> || desert::detail::IsArray<T>)
T deserialize(const QByteArray &json) {
    auto doc = QJsonDocument::fromJson(json);

    if constexpr (desert::detail::isObject<T>()) {
        return deserialize<T>(doc.object());
    } else if constexpr (desert::detail::isArray<T>()) {
        return deserialize<T>(doc.array());
    }
}

namespace detail {

template <typename Value>
QHash<QString, Value> decodeMap(const QJsonValue &value) {
    QHash<QString, Value> map;
    const auto keys = value.toObject().keys();
    for (const auto &key : keys) {
        map.insert(key, deserialize<Value>(value[key]));
    }

    return map;
}

}

///
/// \brief Serialize an object of type to a `QJsonValue`
/// \param object Object to serialize
/// \return Encoded object
///
template <typename T>
requires desert::IsDesertObject<T>
QJsonValue serialize(const T &object);

template <typename T>
constexpr bool isSerializable() {
    return desert::detail::isObject<T>() || desert::detail::isArray<T>() || desert::detail::isMap<T>();
}

template <typename T>
requires (desert::detail::IsArray<T> && !desert::IsDesertObject<T>)
QJsonValue serialize(const T &array) {
    QJsonArray json;

    std::ranges::transform(array, std::back_inserter(json), [](const auto &elem) {
        using ElementType = std::decay_t<decltype(elem)>;
        if constexpr (isSerializable<ElementType>()) {
            return serialize<ElementType>(elem);
        } else {
            return QJsonValue(elem);
        }
    });

    return json;
}

template <typename T>
QJsonValue serialize(const T &value);

template <typename T>
    requires (desert::detail::IsMap<T>)
QJsonValue serialize(const T &map) {
    QJsonObject json;

    for (const auto &key : map.keys()) {
        json[key] = serialize(map[key]);
    }

    return json;
}

template <typename T>
requires desert::IsDesertObject<T>
QJsonValue serialize(const T &object) {
    QJsonObject json;
    desert::iterateStruct(object, [&](auto &attribute) {
        using Attr = std::decay_t<decltype(attribute.value)>;
        if constexpr (isSerializable<Attr>()) {
            json[attribute.attributeName()] = serialize<Attr>(attribute.value);
        } else {
            json[attribute.attributeName()] = QJsonValue(attribute.value);
        }
    });

    return json;
}

template <typename T>
QJsonValue serialize(const T &value) {
    return QJsonValue(value);
}

///
/// \brief Convert a QJSonValue to a byte array.
///
/// This should be used after serializing.
///
/// \param value to convert to bytes
///
DESERT_EXPORT QByteArray toBytes(const QJsonValue &value);

}
